// 2.

db.fruits.aggregate([   
    {$match:{$and:[{supplier:"Yellow Farms"},{price:{$lt:50}}]}},
    {$count:"yellowFarmLowerThan50"}
])

// 3.

db.fruits.aggregate([   
    {$match:{price:{$lt:30}}},
    {$count:"priceLesserThan30"}
])

// 4.

db.fruits.aggregate([
    {$match: {supplier:"Yellow Farms"}},
    {$group: {_id:"Yellow Farms",avgPrice:{$avg:"$price"}}}
])

// 5.

db.fruits.aggregate([
    {$match: {supplier:"Red Farms Inc."}},
    {$group: {_id:"Red Farms Inc.",maxPrice:{$max:"$price"}}}
])

// 6.

db.fruits.aggregate([
    {$match: {supplier:"Red Farms Inc."}},
    {$group: {_id:"Red Farms Inc.",minPrice:{$min:"$price"}}}
])